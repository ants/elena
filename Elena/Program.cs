﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elena
{
    public class KeywordReply
    {
        public List<string> Keywords = new List<string>();
        public List<string> Replies = new List<string>();

        int _currentReply = 0;

        static readonly List<Tuple<string, string>> _conjugate = new List<Tuple<string, string>>
        {
            Tuple.Create(" are "    , " am "    ),
            Tuple.Create(" were "   , " was "   ),
            Tuple.Create(" you "    , " i "     ),
            Tuple.Create(" your "   , " my "    ),
            Tuple.Create(" ive "    , " youve " ),
            Tuple.Create(" im "     , " youre " ),
        };

        string ReverseConjugation(string input)
        {
            foreach(var pair in _conjugate)
            {
                if (input.Contains(pair.Item1))
                    input = input.Replace(pair.Item1, pair.Item2);
                else if (input.Contains(pair.Item2))
                    input = input.Replace(pair.Item2, pair.Item1);
            }
            return input;
        }

        public string FormatReply(string suffix)
        {
            var reply = Replies[_currentReply];
            if (reply.EndsWith("*"))
                reply = reply.Replace("*", "") + ReverseConjugation(suffix);
            _currentReply = (_currentReply + 1) % Replies.Count;
            return reply;
        }
    }

    public class Elena
    {
        readonly TextReader _reader;
        readonly TextWriter _writer;

        readonly List<KeywordReply> _keywordreplies = new List<KeywordReply>
        {
            new KeywordReply()
            {
                Keywords = { "can you" },
                Replies =
                {
                    "Don't you believe that I can*",
                    "Perhaps you would like to be able to*",
                    "You want me to be able to*",
                }
            },
            new KeywordReply()
            {
                Keywords = { "can i" },
                Replies =
                {
                    "Perhaps you want to*",
                    "Do you want to be able to*",
                }
            },
            new KeywordReply()
            {
                Keywords = { "you are", "youre" },
                Replies =
                {
                    "What makes you think I am*",
                    "Does it please you to believe that I am*",
                    "Perhaps you would like to be*",
                    "Do you sometimes wish you were*",
                }
            },
            new KeywordReply()
            {
                Keywords = { "i dont" },
                Replies =
                {
                    "Don't you really*",
                    "Why don't you*",
                    "Do you wish to be able to*",
                    "Does that trouble you?",
                }
            },
            new KeywordReply()
            {
                Keywords = { "i feel" },
                Replies =
                {
                    "Tell me more about such feeling.",
                    "Do you often feel*",
                    "Do you enjoy feeling*",
                }
            },
            new KeywordReply()
            {
                Keywords = { "why dont you" },
                Replies =
                {
                    "Do you really believe I don't*",
                    "Perhaps in good time I will*",
                    "Do you want me to*",
                }
            },
            new KeywordReply()
            {
                Keywords = { "why cant i" },
                Replies =
                {
                    "Do you think you should be able to*",
                    "Why can't you*",
                }
            },
            new KeywordReply()
            {
                Keywords = { "are you" },
                Replies =
                {
                    "Why are you interested in whether or not I am*",
                    "Would you prefer if I were not*",
                    "Perhaps in your fantasies I am*",
                }
            },
            new KeywordReply()
            {
                Keywords = { "i cant" },
                Replies =
                {
                    "How do you know I can't*",
                    "Have you tried?",
                    "Perhaps you can now*",
                }
            },
            new KeywordReply()
            {
                Keywords = { "i am", "im " },
                Replies =
                {
                    "Did you come to me because you are*",
                    "How long have you been*",
                    "Do you believe it is normal to be*",
                    "Do you enjoy being*",
                }
            },
            new KeywordReply()
            {
                Keywords = { "you " },
                Replies =
                {
                    "We were discussing you-- not me.",
                    "Oh, I*",
                    "You're not really talking about me, are you?",
                }
            },
            new KeywordReply()
            {
                Keywords = { "i want" },
                Replies =
                {
                    "What would it mean if you got*",
                    "Why do you want*",
                    "Suppose you soon got*",
                    "What if you never got*",
                    "I sometimes also want*",
                }
            },
            new KeywordReply()
            {
                Keywords = { "what", "how", "who", "where", "when", "why" },
                Replies =
                {
                    "Why do you ask?",
                    "Does that question interest you?",
                    "What answer would please you the most?",
                    "What do you think?",
                    "Are such questions or your mind often?",
                    "What is it you really want to know?",
                    "Have you asked anyone else?",
                    "Have you asked such question before?",
                    "What else comes to mind when you ask that?",
                }
            },
            new KeywordReply()
            {
                Keywords = { "name" },
                Replies =
                {
                    "Names don't interest me.",
                    "I don't care about names. Go on.",
                }
            },
            new KeywordReply()
            {
                Keywords = { "cause" },
                Replies =
                {
                    "Is that the real reason?",
                    "Don't any other reasons come to mind?",
                    "Does that reason explain anything else?",
                    "What other reason migth there be?",
                }
            },
            new KeywordReply()
            {
                Keywords = { "sorry" },
                Replies =
                {
                    "Please don't apologize.",
                    "Apologies are not necessary.",
                    "What feeling do you get when you apologize?",
                    "Don't be so defensive!",
                }
            },
            new KeywordReply()
            {
                Keywords = { "dream" },
                Replies =
                {
                    "What does that dream suggest to you?",
                    "Do you dream often?",
                    "What persons appear in your dreams?",
                    "Are you disturbed by your dreams?",
                }
            },
            new KeywordReply()
            {
                Keywords = { "hello", "hi " },
                Replies =
                {
                    "How do you do... Please state your problem.",
                }
            },
            new KeywordReply()
            {
                Keywords = { "maybe" },
                Replies =
                {
                    "You don't seem quite certain.",
                    "Why the uncertain tone?",
                    "Can't you be more positive?",
                    "You aren't sure?",
                    "Don't you know?",
                }
            },
            new KeywordReply()
            {
                Keywords = { "no" },
                Replies =
                {
                    "Are you just saying that just to be negative?",
                    "You are being a bit negative.",
                }
            },
            new KeywordReply()
            {
                Keywords = { "your" },
                Replies =
                {
                    "Why not?",
                    "Are you sure?",
                    "Why no?",
                    "Why are you concerned about my*",
                }
            },
            new KeywordReply()
            {
                Keywords = { "always" },
                Replies =
                {
                    "What about your own*",
                    "Can you think of a specific example?",
                    "When?",
                }
            },
            new KeywordReply()
            {
                Keywords = { "think" },
                Replies =
                {
                    "What are you thinking of?",
                    "Really, always?",
                    "Do you really think so?",
                    "But you are not sure you*",
                    "Do you doubt*",
                    "In what way?",
                }
            },
            new KeywordReply()
            {
                Keywords = { "alike" },
                Replies =
                {
                    "What resemblance do you see?",
                    "What does the similarity suggest to you?",
                    "What other connections do you see?",
                    "Could there really be some connection?",
                    "How?",
                }
            },
            new KeywordReply()
            {
                Keywords = { "yes" },
                Replies =
                {
                    "You seem quite positive.",
                    "Are you sure?",
                    "I see.",
                    "I understand.",
                }
            },
            new KeywordReply()
            {
                Keywords = { "friend" },
                Replies =
                {
                    "Why do you bring up the topic of friends?",
                    "Do your friends worry you?",
                    "Do your friends pick on you?",
                    "Are you sure you have any friends?",
                    "Do you impose on your friends?",
                    "Perhaps your love for friends worries you?",
                    "Do computers worry you?",
                }
            },
            new KeywordReply()
            {
                Keywords = { "computer" },
                Replies =
                {
                    "Are you talking about me in particular?",
                    "Are you frightened by machines?",
                    "Why do you mention computers?",
                    "What do you think computers have to do with your problem?",
                    "Don't you think computers can help people?",
                    "What is it about machines that worries you?",
                    "Say, do you have psychological problems?",
                }
            },
        };

        readonly KeywordReply NoMatchKeywordReply = new KeywordReply()
        {
            Keywords = { "nokeyfound" },
            Replies =
            {
                "What does that suggest to you?",
                "I see.",
                "I'm not sure I understand you fully.",
                "Come, come. Elucidate your thoughts.",
                "Can you elaborate on that?",
                "That is quite interesting."
            }
        };

        public Elena(TextReader reader, TextWriter writer)
        {
            _reader = reader;
            _writer = writer;
        }

        public string GetInput(string previous)
        {
            do
            {
                var input = _reader.ReadLine()
                                  .ToLower()
                                  .Replace("\'", "");
                input = String.Format(" {0} ", input);

                if (input.Contains("shut"))
                    return null;

                if (input != previous)
                    return input;

                _writer.WriteLine("Please don't repeat yourself!");
            } while (true);
        }

        public Tuple<KeywordReply, string> FindKeyword(string input)
        {
            foreach (var keywordReply in _keywordreplies)
            {
                foreach (var keyword in keywordReply.Keywords)
                {
                    var index = input.IndexOf(keyword);
                    if (index >= 0)
                        return Tuple.Create(keywordReply, input.Substring(index + keyword.Length));
                }
            }
            return Tuple.Create(NoMatchKeywordReply, string.Empty);
        }

        public void Run()
        {
            string input = String.Empty;

            _writer.WriteLine("Hi! I'm Eliza. What is your problem?");
            while ((input = GetInput(input)) != null)
            {
                var keywordData = FindKeyword(input);
                _writer.WriteLine(keywordData.Item1.FormatReply(keywordData.Item2));
            }
            _writer.WriteLine("Shut up...");
        }
    }

    class Program
    {
        static void Main()
        {
            new Elena(Console.In, Console.Out).Run();
        }
    }
}
